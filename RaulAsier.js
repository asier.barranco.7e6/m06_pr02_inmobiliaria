class Inmueble {

    direccion;
    metrosCuadrados;
    referenciaCatastral;
    precioBase;
    foto;
    coordenadasGeograficas;
    estado;
    añoConstruccion;

    constructor(direccion, metrosCuadrados, referenciaCatastral, precioBase, foto, coordenadasGeograficas, estado, añoConstruccion) {

        this.direccion = direccion;
        this.metrosCuadrados = metrosCuadrados;
        this.referenciaCatastral = referenciaCatastral;
        this.precioBase = precioBase;
        this.foto = foto;
        this.coordenadasGeograficas = coordenadasGeograficas;
        this.estado = estado;
        this.añoConstruccion = añoConstruccion;

    };

    getPrecio() {

        let precio = 0.0;
        precio = this.precioBase;
        let añoAntiguedad = 2023 - this.añoConstruccion;

        if (añoAntiguedad > 10 && añoAntiguedad < 20) {
            for (let i = 1; i <= añoAntiguedad - 10; i++) {
                precio *= 0.99;
            };
        };
        return precio;
    };
};

class Vivienda extends Inmueble {

    numBaños;
    numHab;

    constructor(direccion, metrosCuadrados, referenciaCatastral, precioBase, foto, coordenadasgeograficas, estado, añoConstruccion, numBaños, numHab) {

        super(direccion, metrosCuadrados, referenciaCatastral, precioBase, foto, coordenadasgeograficas, estado, añoConstruccion);
        this.numBaños = numBaños;
        this.numHab = numHab;
    };

    getPrecio() {

        return super.getPrecio();
    };

};

class Piso extends Vivienda {

    planta;
    ascensor;
    metrosTerraza;

    constructor(direccion, metrosCuadrados, referenciaCatastral, precioBase, foto, coordenadasgeograficas, estado, añoConstruccion, numBaños, numHab, planta, ascensor, metrosTerraza) {

        super(direccion, metrosCuadrados, referenciaCatastral, precioBase, foto, coordenadasgeograficas, estado, añoConstruccion, numBaños, numHab);
        this.planta = planta;
        this.ascensor = ascensor;
        this.metrosTerraza = metrosTerraza;
    };

    getPrecio() {

        let precio = 0.0;
        precio = super.getPrecio();

        if (this.planta >= 3 && this.ascensor) {
            precio *= 1.03;
        };

        if (this.metrosTerraza > 0) {
            for (let i = 1; i <= this.metrosTerraza; i++) {
                precio += 300;
            };
        };

        return precio;
    };

};

class Casa extends Vivienda {

    tipo;
    metrosJardin;
    piscina;

    constructor(direccion, metrosCuadrados, referenciaCatastral, precioBase, foto, coordenadasGeograficas, estado, añoConstruccion, numBaños, numHab, tipo, metrosJardin, piscina) {

        super(direccion, metrosCuadrados, referenciaCatastral, precioBase, foto, coordenadasGeograficas, estado, añoConstruccion, numBaños, numHab);
        this.tipo = tipo;
        this.metrosJardin = metrosJardin;
        this.piscina = piscina;
    };

    getPrecio() {

        let precio = super.getPrecio();

        if (this.piscina) {
            precio *= 1.04;
        };

        if (this.metrosJardin > 100 && this.metrosJardin < 250) {
            precio *= 1.05;
        } else if (this.metrosJardin >= 250) {
            precio *= 1.09;
        };

        return precio;
    };
};

class Local extends Inmueble {

    numVentanas;
    persianaMetalica;

    constructor(direccion, metrosCuadrados, referenciaCatastral, precioBase, foto, coordenadasGeograficas, estado, añoConstruccion, numVentanas, persianaMetalica) {

        super(direccion, metrosCuadrados, referenciaCatastral, precioBase, foto, coordenadasGeograficas, estado, añoConstruccion);
        this.numVentanas = numVentanas;
        this.persianaMetalica = persianaMetalica;
    };

    getPrecio() {

        let precio = 0.0
        precio = super.getPrecio();

        if (this.metrosCuadrados > 50) {
            precio *= 1.01;
        };

        if (this.numVentanas <= 1) {
            precio *= 0.98;
        } else if (this.numVentanas >= 4) {
            precio *= 1.04;
        };

        return precio;
    };
};
class Restauracion extends Local {

    mobiliario;
    cafetera;
    extractorHumo;

    constructor(direccion, metrosCuadrados, referenciaCatastral, precioBase, foto, coordenadasGeograficas, estado, añoConstruccion, numVentanas, persianaMetalica, extractorHumo, cafetera, mobiliario) {

        super(direccion, metrosCuadrados, referenciaCatastral, precioBase, foto, coordenadasGeograficas, estado, añoConstruccion, numVentanas, persianaMetalica);
        this.mobiliario = mobiliario;
        this.cafetera = cafetera;
        this.extractorHumo = extractorHumo;
    };

    getPrecio() {

        return super.getPrecio();
    };
};

class Comerciales extends Local {

    adaptado;

    constructor(direccion, metrosCuadrados, referenciaCatastral, precioBase, foto, coordenadasGeograficas, estado, añoConstruccion, numVentanas, persianaMetalica, adaptado) {

        super(direccion, metrosCuadrados, referenciaCatastral, precioBase, foto, coordenadasGeograficas, estado, añoConstruccion, numVentanas, persianaMetalica);
        this.adaptado = adaptado;
    };

    getPrecio() {

        return super.getPrecio();
    };
};

class Industriales extends Local {

    sueloUrbano;
    puertoCargaDescarga;

    constructor(direccion, metrosCuadrados, referenciaCatastral, precioBase, foto, coordenadasGeograficas, estado, añoConstruccion, numVentanas, persianaMetalica, sueloUrbano, puertoCargaDescarga) {

        super(direccion, metrosCuadrados, referenciaCatastral, precioBase, foto, coordenadasGeograficas, estado, añoConstruccion, numVentanas, persianaMetalica);
        this.sueloUrbano = sueloUrbano;
        this.puertoCargaDescarga = puertoCargaDescarga;
    };

    getPrecio() {

        let precio = 0.0;
        precio = super.getPrecio();

        if (this.sueloUrbano) {
            precio *= 1.25;
        };

        return precio;
    };
};

class Usuario {

    nombreUsuario;

    constructor(nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    };

    listarInmuebles() {
        console.log("Lista de inmuebles")
        console.log("");
        let i = 0;
        for (const inmueble of inmuebles) {
            i++
            var isCasa = inmueble instanceof Casa;
            var isPiso = inmueble instanceof Piso;
            var isLocal = inmueble instanceof Local;
            console.log("-----INMUEBLE " + i + "------");
            if (isCasa) {
                console.log("Este inmueble es una casa");
            } else if (isPiso) {
                console.log("Este inmueble es un piso");
            } else if (isLocal) console.log("Este inmueble es un local");

            for (const property in inmueble) {
                console.log(property + ": " + inmueble[property]);
            };
            console.log("Precio final a pagar: " + inmueble.getPrecio() + "€")
            console.log("--------------")
            console.log("");


        };
        console.log("");

    };

    verInmueble(inmuebleDeseado) {
        console.log("");
        console.log("----------INMUEBLE DESEADO----------------");
        for (const inmueble of inmuebles) {
            if (inmuebleDeseado == inmueble.referenciaCatastral) {
                for (const property in inmueble) {
                    console.log(property + ": " + inmueble[property]);
                };
                console.log("Precio final a pagar: " + inmueble.getPrecio() + "€")
            };
        };
        console.log("-------------------------------------------");
        console.log("");
    };

    filtrarInmuebles(tipo) {
        console.log("");
        console.log("--------Inmuebles que son " + tipo + "-------");
        console.log("");
        let i = 0;
        for (const inmueble of inmuebles) {
            let info = ""
            let destino = document.getElementById("info")
            if (this.esTipo(inmueble, tipo)) {
                i++
                console.log("-----" + tipo + " " + i + "------");
                for (const property in inmueble) {
                    console.log(property + " :" + inmueble[property]);
                };
                console.log("Precio final a pagar: " + inmueble.getPrecio() + "€")
                console.log("");
            };
        };
        console.log("-------------------------------------------");
    };

    esTipo(inm, tipo) {
        if (tipo == "Casa" && inm instanceof Casa) {
            return true;
        } else if (tipo == "Piso" && inm instanceof Piso) {
            return true;
        } else if (tipo == "Local" && inm instanceof Local) {
            return true;
        } else return false;
    };

};

function crearInmuebles(){

let Piso1 = new Piso("Carrer Gran de Sant Andreu, 468", 200, "00123A001B", 200000, "https://media.revistaad.es/photos/60c226b5c13d5c8c1282e8d3/master/pass/251244.jpg", "41.442072761515185, 2.1879381752594402", "Recién reformado", 2003, 2, 4, 5, true, 43);
let Piso2 = new Piso("Passeig Fabra i Puig, 124", 120, "98765B432A", 176000, "https://media.yaencontre.com/img/photo/w380/55916/55916-44769336-1166527388.jpg", "41.43085106502952, 2.189594461720193", "Para entrar a vivir", 2014, 3, 3, 3, true, 23);
let Piso3 = new Piso("Carrer Joan Torres, 65", 86, "55555Z123X", 130000, "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSdxFWMzwh9s3W3uZ-BhcMzW2ZB2KkBRMSwNg&usqp=CAU", "41.44278830841187, 2.193064857413034", "De segunda mano", 1976, 2, 3, 6, true, 12);

let Casa1 = new Casa("Carrer de Grau, 12", 240, "4321F678T", 345000, "https://www.proyectosdecasas.es/files/images/Casa_de_lujo_Barcelona.JPG", "41.43981250645772, 2.187556630456514", "Necesita una reforma", 1989, 3, 5, "Con parcela", 550, false);
let Casa2 = new Casa("Carrer Aiguablava, 121", 178, "9090H33K2", 235000, "https://phantom-expansion.unidadeditorial.es/cc2895bc5b6905f8d15578ae0689692d/crop/160x0/1543x924/resize/1200/f/webp/assets/multimedia/imagenes/2022/04/27/16510836963156.jpg", "41.450090501488916, 2.187685376421741", "Como nueva", 2000, 3, 5, "Adosada", 80, true);
let Casa3 = new Casa("Passeig Torres i Bages, 35", 450, "77777D111S", 675000, "https://assets-global.website-files.com/5f4f67c5950db17954dd4f52/5f5b7ee442f1e5b9fee1c117_hacerse-una-casa.jpeg", "41.44201622004133, 2.191268807529851", "Impecable", 2020, 5, 7, "Lujosa", 250, true);

let Bar1 = new Restauracion("Carrer Pons i Casal, 4", 100, "4444L678T", 210500, "https://media.traveler.es/photos/61375e9abae07f0d8a492331/master/w_1600%2Cc_limit/214105.jpg", "41.43657926492813, 2.1876445891403526", "Seminuevo", 1987, 3, true, true, true, true);
let Gestoria1 = new Comerciales("Plaça Orfila, 4", 75, "2468K135M", 98450, "https://images.theconversation.com/files/349727/original/file-20200727-37-592phd.jpg?ixlib=rb-1.1.0&rect=359%2C159%2C4967%2C3386&q=20&auto=format&w=320&fit=clip&dpr=2&usm=12&cs=strip", "41.43230009509925, 2.1870223166724774", "Perfectas condiciones", 2005, 5, false, true);
let Reformas1 = new Industriales("Carrer Malats, 93", 65, "88888N654Y", 145600, "https://instalacionesbarcelona.net/wp-content/uploads/2019/11/Reforma-de-locales-comerciales-en-Barcelona.jpg", "41.435517542337095, 2.187301266399456", "Condiciones no favorables", 2005, 4, true, true, true);

}


const API_KEY_MAPBOX = `pk.eyJ1IjoiZWxvaS1pdGIiLCJhIjoiY2xma3RkaWJtMGU0bjQ2czQ0aDVtb2oxYiJ9.KCp4ICW9sYHRvQIxHBpzzQ`

const dom_address = document.getElementById("address")

//LEAFLET
var map = L.map('map').setView([40.34654412118006,-3.7792968750000004], 6);


L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
 attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

// L.marker([44.846814, -0.575341]).addTo(map)
// .bindPopup('A pretty CSS3 popup.<br> Easily customizable.')
// .openPopup();


map.on('click', async function(e){
 var coord = e.latlng;
 var lat = coord.lat;
 var lng = coord.lng;
 console.log("You clicked the map at latitude: " + lat + " and longitude: " + lng);

 const geoData = await mapbox_reverseGeocode(lat,lng)

 console.log(geoData)
 setLocation(geoData)
});


//MAPBOX
async function mapbox_geocode(query){
 query = encodeURIComponent(query)

 const url = `https://api.mapbox.com/geocoding/v5/mapbox.places/${query}.json?access_token=${API_KEY_MAPBOX}`

 const response = await fetch(url)
 const data = await response.json()

 return data
}

function getTextLocationFromMapboxData(mapbox_data){
 const feature = mapbox_data.features[0]

 return feature.text + " " + feature.address + "<br>" + feature.context.map(x=>x.text).join(", ")
}


function setLocation(geoData){
 const address = getTextLocationFromMapboxData(geoData)
 const coords = geoData.features[0].center.reverse()

 map.setView(coords,13)

 L.marker(coords).addTo(map)
 .bindPopup(address)
 .openPopup();

 dom_address.value = address.replace("<br>","; ")
}

//formulario
const form_locate = document.getElementById("locate")
form_locate.onsubmit=async (event)=>{
 event.preventDefault()
 const data = new FormData(form_locate)

 console.log((data.get("address")))
 const geoData = await mapbox_geocode(data.get("address"))

 setLocation(geoData)
}



const tipo = document.getElementById("selectipo")
const comun = document.getElementById("comun")
const piso = document.getElementById("piso")
const casa = document.getElementById("casa")
const restauracion = document.getElementById("restauracion")
const comerciales = document.getElementById("comerciales")
const industriales = document.getElementById("industriales")
const boton = document.getElementById('envia')
const mapa = document.getElementById('map')


comun.style.display = "none";
casa.style.display = "none";
piso.style.display = "none";
restauracion.style.display = "none";
comerciales.style.display = "none";
industriales.style.display = "none";
mapa.style.display = "none";
function mostrarmapa(){
    mapa.style.display = "block"
}
boton.onclick = mapa.style.display = mostrarmapa
tipo.addEventListener("change", () => {
    mapa.style.display = "none"
    boton.style.display = "block";
    if (tipo.value === "piso") {

        comun.style.display = "block";
        piso.style.display = "block";
        casa.style.display = "none";
        restauracion.style.display = "none";
        comerciales.style.display = "none";
        industriales.style.display = "none";

    } else if (tipo.value === "casa") {

        comun.style.display = "block";
        casa.style.display = "block";
        piso.style.display = "none";
        restauracion.style.display = "none";
        comerciales.style.display = "none";
        industriales.style.display = "none";

    } else if (tipo.value === "restauracion") {

        comun.style.display = "block";
        restauracion.style.display = "block";
        casa.style.display = "none";
        piso.style.display = "none";
        comerciales.style.display = "none";
        industriales.style.display = "none";

    } else if (tipo.value === "comerciales") {

        comun.style.display = "block";
        comerciales.style.display = "block";
        casa.style.display = "none";
        piso.style.display = "none";
        restauracion.style.display = "none";
        industriales.style.display = "none";

    } else if (tipo.value === "industriales") {

        comun.style.display = "block";
        industriales.style.display = "block";
        piso.style.display = "none";
        casa.style.display = "none";
        restauracion.style.display = "none";
        comerciales.style.display = "none";

    } else if (tipo.value === "none") {

        comun.style.display = "none";
        casa.style.display = "none";
        piso.style.display = "none";
        restauracion.style.display = "none";
        comerciales.style.display = "none";
        industriales.style.display = "none";
        boton.style.display = "none"

    }
});
